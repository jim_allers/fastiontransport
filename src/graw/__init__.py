import numpy
import scipy.interpolate

# Default data type perform calculations
_dtype = numpy.float64
_cdtype = numpy.complex128


# -----------------------------------------------------------------------------
# Interpolation functions
#
class Interpolation(object):
    """Class for interpolation functions using data sampled over a
    discrete mesh of points.  If the mesh contains four or more samples, then
    cubic interpolation is used.  Otherwise, linear interpolation is used.

    :param mesh: Mesh of points sampling the dependent variable.
    :type mesh: array_like
    :param sample: values of the independent variable sampled over `mesh`.
    :type sample: array_like
    """

    def __init__(self, mesh, sample, dtype=_dtype, bounds_error=True):
        self.mesh = numpy.asarray(mesh, dtype=dtype)
        self.sample = numpy.asarray(sample, dtype=dtype)

        # Use cubic interpolation if there are enough sample points
        if len(self.mesh) > 3:
            self.func = scipy.interpolate.interp1d(
                self.mesh,
                self.sample,
                kind='cubic',
                bounds_error=bounds_error
            )
            self.spline = scipy.interpolate.InterpolatedUnivariateSpline(
                self.mesh,
                self.sample,
                k=3
            )

        # Otherwise use linear interpolation
        else:
            self.func = scipy.interpolate.interp1d(
                self.mesh,
                self.sample,
                kind='linear',
                bounds_error=bounds_error
            )
            self.spline = scipy.interpolate.InterpolatedUnivariateSpline(
                self.mesh,
                self.sample,
                k=1
            )

        return None

    def __call__(self, coor):
        return self.func(coor)


def resample(sample, mesh_old, mesh_new):
    """Resample a set of values originally on an old mesh of coordinates to
    match a new mesh of coordinates.  The new points are interpolated using a
    cubic spline.

    :param sample: values of the independent variable sampled over `mesh_old`
    :type sample: array_like
    :param mesh_old: old mesh over the dependent variable
    :type mesh_old: array_like
    :param mesh_new: new mesh over the dependent variable
    :type mesh_new: array_like
    """
    func = Interpolation(mesh_old, sample)
    sample_new = func(mesh_new)
    return sample_new
