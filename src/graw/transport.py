# transport.py
#
# Copyright (C) 2014 - True Merrill <true.merrill@gtri.gatech.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""
Class for transport function solutions
"""

import numpy

from numpy import pi
from . import _dtype, Interpolation


class Transport_function(object):
    r"""A class to hold representations of the transport function in the
    position representation.  The wavefunction consists of a real part
    :math:`\psi_R(x, t)` and an imaginary part :math:`\psi_I(x, t)`, each of
    which is represented by a real-valued ``numpy.float64`` array.

    ..  attribute:: time

        Mesh over the time coordinate

    ..  attribute:: sample

        Discrete values of the transport function sampled over the `time` mesh
    """

    def __init__(self, time=None, sample=None, dtype=_dtype):

        # ---------------------------------------------------------------------
        # For each keyword argument, check if the user provided input.  If not
        # initialize the Transport_function using the default.
        #
        if time == None:
            time = numpy.linspace(0., 2 * pi, 2 ** 8 + 1)
            self.time = numpy.asarray(time, dtype=dtype)
        else:
            self.time = time
        self.shape = time.shape

        if sample == None:
            # Default to a linear interpolation between 0 and 1.
            sample = numpy.linspace(0., 1., len(self.time))
            self.sample = numpy.asarray(sample, dtype=dtype)
        else:
            sample = numpy.asarray(sample, dtype=dtype)
            if not sample.shape == self.shape:
                raise ValueError("shape mismatch: the mesh, and sample " +
                    "arrays must have the same shape"
                )
            self.sample = sample

        return None

    def __call__(self, t):
        # Create an interpolation function, only if we haven't already created
        # one
        if hasattr(self, '_interp'):
            interp = self._interp
        else:
            interp = Interpolation(self.time, self.sample)
            self._interp = interp
        return interp(t)


def transport_function_derivative(transport_function):
    """Function to take the partial derivative of the transport function with
    respect to time coordinate.  Returns a new :class:`Transport_function`
    instance representing the derivative.

    :param transport_function: Transport function for the problem.
    :type transport_function: :class:`Transport_function`
    """
    dt = numpy.diff(transport_function.time)
    h = numpy.mean(dt)

    if hasattr(transport_function, '_func'):
        interp = transport_function._func
    else:
        # Construct interpolation object
        interp = Interpolation(
            transport_function.time, transport_function.sample
        )

    def derivative(f, x, h):
        """First-order partial derivative, using five point stencil"""
        result = - f(x + 2 * h) + 8. * f(x + h) - 8. * f(x - h) + f(x - 2 * h)
        result = result / (12. * h)
        return result

    # Compute partial derivative
    time = transport_function.time
    sample = derivative(interp.spline, time, h)
    result = Transport_function(time, sample)
    return result


class Potential_function(object):
    r"""A class representing time-dependent axial potentials in the co-moving
    frame.  We represent axial potentials with the Taylor series,

    ..  math::

        V(x, t) = \sum_{n = 1}^\infty c_n( x_0(t), t) x^n

    where :math:`x_0(t)` is the transport function, and :math:`c_n(x_0(t), t)`
    are time-dependent coefficients that describe the changing shape of the
    co-moving potiential well.

    The coefficients are sampled on a mesh of time points.  This mesh must
    match the `time` mesh for a corresponding transport function.  With a
    potential function and transport function, the Hamiltonian of the transport
    problem is completely specified.

    ..  attribute:: time

        mesh over the time coordinate

    ..  attribute:: coeff
    """

    def __init__(self, time=None, coeff=None, dtype=_dtype):

        self._dtype = dtype

        # ---------------------------------------------------------------------
        # For each keyword argument, check if the user provided input.  If not
        # initialize the Potential_function using the default.
        #
        if time == None:
            time = numpy.linspace(0., 2 * pi, 2 ** 8 + 1)
            self.time = numpy.asarray(time, dtype=dtype)
        else:
            self.time = time
        self.shape = time.shape

        if coeff == None:
            # Default to a harmonic potential
            coeff = numpy.zeros((2, len(self.time)), dtype=dtype)
            coeff[1, :] = 1.
            self.coeff = coeff

        else:
            self.coeff = coeff

        # ---------------------------------------------------------------------
        # For each coefficient, create an interpolation function.  The
        # interpolation function allows us to resample the coefficients on a
        # different mesh of time points.  This allows us to use a small step
        # size for FDTD, but only specify the coefficient along a courser grid.
        #
        coeff_funcs = []
        for index in range(self.coeff.shape[0]):
            coeff_slice = self.coeff[index, :]
            func = Interpolation(self.time, coeff_slice)
            coeff_funcs.append(func)

        self.coeff_funcs = numpy.asarray(coeff_funcs)

        return None

    def __call__(self, time, space):
        result = self.sample_mesh(time, space)
        if result.shape[0] == 1:
            return result.flatten()
        else:
            return result

    def sample_mesh(self, time, space):
        """Samples the potential function over a mesh of time and spatial
        points.  Returns a `(n, m)` array representing the value of the
        potential function, where `n` is the number of time coordinate
        samples
        """
        xv, tv = numpy.meshgrid(space, time)
        sample = 0. * tv

        # Add the terms of the potential, coefficient by coefficient
        for index in range(len(self.coeff_funcs)):
            n = index + 1
            c = self.coeff_funcs[index]
            sample += c(tv) * (xv ** n)

        return sample
