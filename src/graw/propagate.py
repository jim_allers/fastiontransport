# propagate.py
#
# Copyright (C) 2014 - True Merrill <true.merrill@gtri.gatech.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import numpy
from datetime import datetime
import wavefunction as wv
import transport as trans

from numpy import (pi, sqrt, exp, arange, linspace, real, imag)
from numpy.fft import (fft, ifft, fftfreq)
from scipy.integrate import complex_ode

class Problem(object):
    """Class to store parameters for ion transport problems.  In the following,
    ``x`` refers to the array of position coordinates, ``t`` refers to the
    array of time coordinates, and ``k`` refers to the reciprocal space array
    of momentum coordinates.

    ..  attribute:: length

        Total span of the spatial coordinate mesh in the simulation, e.g. the
        length is ``x[-1] - x[0]``.

    ..  attribute:: samples

        Number of samples in the position coordinate mesh, e.g. the number of
        samples is ``len(x)``.

    ..  attribute:: length

        Mesh over spatial coordinates for the simulation.

    ..  attribute:: dx

        Mesh increment for the space coordinate.

    ..  attribute:: x0

        Mesh offset for the space coordinate.

    ..  attribute:: start_time

        Initial start time for the simulation, e.g. the start time is ``t[0]``.

    ..  attribute:: stop_time

        Final time for the simulation.  The stop time is ``t[-1]``.

    ..  attribute:: dt

        Time increment for the ODE integrator.  Usually the time step is
        ``t[n] - t[n-1]``, however when ``n`` corresponds to the final time
        sample the actual increment may be smaller.

    ..  attribute:: time

        Mesh over time coordinate for the simulation.

    ..  attribute:: momentum

        Mesh over momentum coordinates for the simulation.  The ordering of the
        momentum space indices matches the :py:`fftfreq` convention.

    ..  attribute: dk

        Mesh increment for the momentum coordinate.
    """

    def __init__(self, length=60, samples=2 ** 9, start_time=0,
        stop_time=2 * pi, dt=2 * pi * 5e-3, psi=None,
        potential_function=None, transport_function=None, callback=None,
        store_wavefunctions=False, display=False):

        # ---------------------------------------------------------------------
        # Construct meshes over the position coordinate, time coordinate, and
        # construct a reciprocal-space mesh for the momentum coordinate used
        # in Fast-Fourier Transforms.
        #

        # Create mesh over the position coordinate
        self.length = float(length)
        self.samples = int(samples)
        self.position = linspace(
            - self.length / 2., self.length / 2., self.samples
        )

        # Create mesh over the time coordinate
        self.start_time = float(start_time)
        self.stop_time = float(stop_time)
        self.dt = float(dt)
        time = arange(self.start_time, self.stop_time, self.dt)

        # Ensure the final time is in the mesh of times
        if time[-1] == self.stop_time:
            self.time = time
        else:
            time = numpy.append(time, self.stop_time)
            self.time = time

        # Create the Fourier mesh
        k, dk, x, dx, x0, n = _fft_mesh(self.position)  # @UnusedVariable
        self.momentum = k
        self.dk = dk
        self.dx = dx
        self.x0 = x0

        # ---------------------------------------------------------------------
        # Check other inputs
        #
        if psi == None:
            self.psi = wv.harmonic_oscillator(0, self.position)
        else:
            self.psi = psi

        if potential_function == None:
            self.potential_function = trans.Potential_function()
        else:
            self.potential_function = potential_function

        if transport_function == None:
            self.transport_function = trans.Transport_function()
        else:
            self.transport_function = transport_function

        # Take the derivative of the transport function
        self.transport_derivative = trans.transport_function_derivative(
            self.transport_function
        )

        self.store_wavefunctions = store_wavefunctions
        self.callback = callback
        self.display = display
        return None

    def rhs(self, t, psi):
        """Right-hand side of the Schrodinger equation ODE"""
        return - 1j * hamiltonian(
            t, psi, self.position, self.dx, self.x0, self.samples,
            self.transport_derivative, self.potential_function
            )

    def solve(self):
        """Solve the problem by integrating the Schrodinger equation"""
        # Set up the ODE solver
        self.ode = complex_ode(f=self.rhs)
        self.ode.set_integrator('vode', with_jacobian=False)
        self.ode.set_initial_value(self.psi, self.start_time)

        if self.display:
            import matplotlib.pyplot as plt
            plt.ion()
            fig, axes = plt.subplots(3, sharex=True)
            psi_real, = axes[0].plot(self.position, real(self.psi), 'b-')
            psi_imag, = axes[0].plot(self.position, imag(self.psi), 'r--')
            axes[0].set_ylim([-0.8, 0.8])
            axes[0].set_ylabel(r'$\langle x | \psi(t) \rangle')
            prob, = axes[1].plot(
                self.position, wv.probability_density(self.psi), 'k'
                )
            axes[1].set_ylim([0, 1])
            axes[1].set_ylabel(r'Probability density')
            _pe = self.potential_function(self.start_time, self.position)
            potential, = axes[2].plot(
                self.position,
                _pe,
                'k'
                )
            axes[2].set_ylabel(r'Potential energy')
            axes[2].set_xlabel(r'Position')
            axes[2].set_ylim([numpy.min(_pe), self.potential_function(self.start_time, 10)])
            fig.canvas.draw()

        # Step through time steps.  Initialize storage for wavefunctions if
        # requested by user.
        if self.store_wavefunctions:
            data = numpy.zeros((len(self.time), len(self.psi)), dtype=complex)

        for index, t in enumerate(self.time[1:]):
            self.ode.integrate(t)
            psi = self.ode.y
            if self.store_wavefunctions:
                data[index] = psi

            if not self.callback == None:
                self.callback(psi, self.position, t)

            if self.display:
                psi_real.set_ydata(real(psi))
                psi_imag.set_ydata(imag(psi))
                prob.set_ydata(wv.probability_density(psi))
                potential.set_ydata(
                    self.potential_function(t, self.position)
                )
                _pe = self.potential_function(t, self.position)
                axes[2].set_ylim([numpy.min(_pe), self.potential_function(t, 10)])
                fig.canvas.draw()

        if self.store_wavefunctions:
            return data
        else:
            return psi


def _fft_check(x, tol=1e-14):
    """Check the mesh of position coordinates to ensure the function is
    symmetrically sampled about the origin, and that the mesh is regular.

    :param x: mesh of position coordinates
    :type x: array_like
    """
    if not (len(x) % 2) == 0:
        message = 'Position mesh does not contain an even number of points'
        raise ValueError(message)

    dx = numpy.diff(x)[0]
    if not (abs(numpy.diff(x) - dx) < tol).all():
        raise ValueError('Position mesh not regularly sampled')

    x1 = min((abs(x) < dx) * x) + max((abs(x) < dx) * x)
    if not x1 < tol:
        message = 'Position mesh not symmetrically sampled about the origin'
        raise ValueError(message)

    return None


def _fft_mesh(x):
    """Create a mesh for the conjugate momentum variable using the
    corresponding mesh of position samples.

    :param x: mesh of position coordinates
    :type x: array_like
    """
    _fft_check(x)

    n = len(x)                      # Number of samples
    x0 = x[0]                       # Position mesh offset
    L = x[-1] - x[0]                # Length of position mesh
    dx = L / (n - 1.)               # Position mesh spacing
    dk = 2 * pi / (n * dx)          # Momentum mesh spacing
    k = 2 * pi / dx * fftfreq(n)    # Momentum mesh
    return k, dk, x, dx, x0, n


def to_momentum(psi, dx, x0, n):
    """Transform a wavefunction in the position representation to the momentum
    representation using Fast-Fourier Transforms.

    :param x: mesh of position coordinates
    :type x: array_like
    :param psi: wavefunction samples in the position representation
    :type psi: array_like
    """
    # Take the discrete Fourier transform
    k = 2 * pi / dx * fftfreq(n)
    psip = dx * (exp(-1j * k * x0) / sqrt(2 * pi)) * fft(psi)
    return psip


def to_position(psi, dx, x0, n):
    """Transform a wavefunction in the momentum representation to the position
    representation using an inverse Fast-Fourier Transform.

    :param k: mesh of momentum coordinates
    :type k: array_like
    :param psi: wavefunction samples in the momentum representation
    :type psi: array_like
    """
    # Take the discrete inverse Fourier transform
    k = 2 * pi / dx * fftfreq(n)
    psix = sqrt(2 * pi) / dx * ifft(psi * exp(1j * k * x0))
    return psix


def hamiltonian(t, psi, x, dx, x0, n, dx0, V):
    """Compute the element :math:`\langle x | \hat{H}(t) | \psi(t) \rangle` for
    the co-moving transport Hamiltonian,

    ..  math::

        H(t) = \frac{1}{2} \hat{k}^2 - \dot{x}_0(t) \hat{k} + V(\hat{x}, t)

    where :math:`\hbar \hat{k}` is the linear momentum operator,
    :math:`\hat{x}` is the position operator, :math:`x_0(t)` is the transport
    function, and :math:`V(\hat{x}, t)` is a position-dependent potential.

    :param psi: wavefunction samples in the position representation.
    :type psi: array_like
    :param x: mesh of position coordinates
    :type x: array_like
    :param k: mesh of momentum coordinates
    :type k: array_like
    :param dx0: derivative of the transport function
    :type dx0: callable
    :param V: potential function returning an array of potential energies
        sampled over the position mesh
    :type V: callable
    """
    start = datetime.now()
    energy = numpy.zeros(x.shape, dtype=complex)

    # Position part
    energy += V(t, x) * psi

    # Momentum part
    k = 2 * pi / dx * fftfreq(n)
    psip = to_momentum(psi, dx, x0, n)
    psip = (k ** 2 / 2. - dx0(t) * k) * psip
    energy += to_position(psip, dx, x0, n)
    end = datetime.now()
    print 'elapsed time: {}'.format(end - start)
    return energy


#
# TODO: Make the Jacobian function faster by subclassing ndarray.  Most of the
# entries are repeats, e.g. J_{i j} = J_{i (s-i)} and J only depends on s.
# It might be faster to *emulate* a matrix by overriding the __getitem__()
# method.
#

def jacobian(t, x, dx, x0, n, dx0, V):
    """Compute the Jacobian of the Hamiltonian in the position representation.
    The Jacobian is an `n x n` matrix where `n` is the number of position
    samples.  The matrix entries are

    ..  math::

        J_{ij}(t) = - i \langle x_i | \hat{H}(t) | x_j \rangle

    where :math:`x_i` is the `i`th sample of the position coordinate and
    :math:`\hat{H}(t)` is the Hamiltonian in the co-moving frame.

    :param x: mesh of position coordinates
    :type x: array_like
    :param k: mesh of momentum coordinates
    :type k: array_like
    :param dx0: derivative of the transport function
    :type dx0: callable
    :param V: potential function returning an array of potential energies
        sampled over the position mesh
    :type V: callable
    """
    # Position part
    position_part = -1j * V(t, x)

    if not hasattr(position_part, 'len'):
        V = numpy.vectorize(V)
        position_part = -1j * V(t, x)

    elif not len(position_part) == len(x):
        V = numpy.vectorize(V)
        position_part = -1j * V(t, x)

    # Momentum part
    k = 2 * pi / dx * fftfreq(n)
    al = (k ** 2 / 2. - dx0(t) * k)
    momentum_part = - 1j * n / (2 * pi) * ifft(al)

    def J(i, j):
        """Function to fill out matrix elements"""
        s = i - j
        jac = momentum_part[s]
        if s == 0:
            jac += position_part[j]
        return jac

    # Construct the Jacobian matrix
    jac = numpy.zeros((n, n), dtype=complex)
    for i in range(n):
        for j in range(n):
            jac[i, j] = J(i, j)

    return jac
