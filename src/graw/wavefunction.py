# wavefunction.py
#
# Copyright (C) 2014 - True Merrill <true.merrill@gtri.gatech.edu>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""
Class for wavefunction solutions
"""

import numpy
import scipy.integrate

from numpy import (pi, exp, sqrt, real, imag, conj)
from scipy.misc import factorial  # @UnresolvedImport
from numpy.polynomial.hermite import hermval
from . import _cdtype


# -----------------------------------------------------------------------------
# Wavefunction constructor functions
#
def harmonic_oscillator(n, x):
    r"""Function returning the harmonic oscillator eigenfunction
    :math:`\langle x | n \rangle` in the position representation sampled over
    the array of position coordinates `x`.

    :param n: Quantum number of the harmonic oscillator eigenfunction.
    :type n: ``int``
    :param x: Array of position coordinates to sample the wavefunction over.
    :type x: array_like
    """
    # Create list of Hermite coefficients
    s = [0.] * (n + 1)
    s[n] = 1.

    # Calculate wavefunction
    wave = numpy.zeros(x.shape, dtype=_cdtype)
    wave += 1 / sqrt(2 ** n * factorial(n)) * pi ** (-1. / 4) * \
        exp(- x ** 2 / 2.) * hermval(x, s)

    return wave


def coherent_state(alpha, x):
    r"""Function returning the coherent state :math:`\langle x|\alpha \rangle`
    in the position representation sampled over the array of position
    coordinates `x`.

    :param alpha: Displacement phase.
    :type alpha: ``complex``
    :param x: Array of position coordinates to sample the wavefunction over.
    :type x: array_like
    """
    # Calculate position and momentum shifts
    dx = sqrt(2) * real(alpha)
    dk = - sqrt(2) * imag(alpha)

    # Calculate wavefunction in position representation
    wave = numpy.zeros(x.shape, dtype=_cdtype)
    wave += (pi) ** (-1. / 4) * exp(-1. / 2 * ((x - dx) ** 2 + \
        2 * 1j * x * dk - dk * dx))

    return wave


def squeezed_state(xi, x):
    r"""Function returning the squeezed state :math:`\langle x | \xi \rangle`
    in the position representation sampled over the array of position
    coordinates `a`.

    ..  todo::

        Finish `squeezed_state` function

    :param xi: Squeeze parameter.
    :type xi: ``complex``
    :param x: Array of position coordinates to sample the wavefunction over.
    :type x: array_like
    """
    return NotImplemented


# -----------------------------------------------------------------------------
# Functions acting on wavefunctions
#

def inner_product(psi1, psi2, x):
    r"""Compute the wavefunction inner product

    ..  math::

        \langle \psi_1 | \psi_2 \rangle = \int_{-\infty}^{\infty} d x
        \langle \psi_1| x \rangle \langle x | \psi_2 \rangle.

    :param psi1: First wavefunction
    :type psi1: array_like
    :param psi2: Second wavefunction
    :type psi2: array_like
    :param x: Array of position coordinates.
    :type x: array_like
    """
    ip = scipy.integrate.simps(conj(psi1) * psi2, x)
    return ip


def probability_density(psi):
    r"""Compute the probability density function for the wavefunction,
    :math:`f(x) = |\langle x | \psi \rangle|^2`.

    :param psi1: Wavefunction
    :type psi1: array_like
    """
    pdf = conj(psi) * psi
    return pdf


def normalize(psi, x):
    r"""Normalize a wavefunction so that it's inner product with itself
    is exactly one.

    :param psi1: Wavefunction
    :type psi1: array_like
    :param x: Array of position coordinates.
    :type x: array_like
    """
    norm = inner_product(psi, psi, x)
    norm_psi = psi / sqrt(real(norm))
    return norm_psi

'''
class Wavefunction(object):
    r"""A class to hold representations of the motional wavefunction in the
    position representation.  The wavefunction consists of a real part
    :math:`\psi_R(x, t)` and an imaginary part :math:`\psi_I(x, t)`, each of
    which is represented by a real-valued ``numpy.float64`` array.

    ..  attribute:: space

        Mesh over the spatial coordinates

    ..  attribute:: real

        Real part of the wavefunction

    ..  attribute:: imag

        Imaginary part of the wavefunction
    """

    def __init__(self, space=None, real=None, imag=None, dtype=_dtype):

        # ---------------------------------------------------------------------
        # For each keyword argument, check if the user provided input.  If not
        # initialize the wavefunction using the default.
        #
        if space == None:
            space = numpy.linspace(-10., 10., 2 ** 10 + 1)
            self.space = numpy.asarray(space, dtype=dtype)
        else:
            self.space = space
        self.shape = space.shape

        if real == None:
            self.real = numpy.array(
                harmonic_oscillator(0, self.space), dtype=dtype
            )
        else:
            real = numpy.asarray(real, dtype=dtype)
            if not real.shape == self.shape:
                raise ValueError("shape mismatch: the space, real, and imag " +
                    "arrays must have the same shape"
                )
            self.real = real

        if imag == None:
            self.imag = numpy.zeros(self.real.shape, dtype=dtype)
        else:
            imag = numpy.asarray(imag, dtype=dtype)
            if not real.shape == self.shape:
                raise ValueError("shape mismatch: the mesh, real, and imag " +
                    "arrays must have the same shape"
                )
            self.imag = imag

        # normalize wavefunction
        self.normalize()

        return None

    def __call__(self, t):
        # Create an interpolation function, only if we haven't already created
        # one
        if hasattr(self, '_interp_real') and hasattr(self, '_interp_imag'):
            interp_real = self._interp_real
            interp_imag = self._interp_imag
        else:
            interp_real = Interpolation(self.space, self.real)
            interp_imag = Interpolation(self.space, self.imag)
            self._interp_real = interp_real
            self._interp_imag = interp_imag

        return interp_real(t) + 1j * interp_imag(t)

    def inner_product(self, target):
        """Method to take the inner product between two wavefunctions.

        :param target: target wavefunction to take inner product with
        :type target: :class:`Wavefunction`
        """

        if not (self.space == target.space).all():
            raise ValueError("space mismatch: the wavefunctions are sampled " +
                "over different meshes"
            )

        real_part = scipy.integrate.simps(
            target.real * self.real + target.imag * self.imag,
            self.space,
            even='avg'
        )

        imag_part = scipy.integrate.simps(
            target.real * self.imag - target.imag * self.real,
            self.space,
            even='avg'
        )

        return real_part + 1j * imag_part

    def normalize(self):
        """Method to normalize wavefunction."""
        inner_product = self.inner_product(self)
        norm = abs(inner_product)
        self.real *= 1 / sqrt(norm)
        self.imag *= 1 / sqrt(norm)
        return None

    def prob(self):
        """Method returning probability density of wavefunction."""
        prob_density = self.real ** 2 + self.imag ** 2
        return prob_density
'''
