from graw.propagate import *
from graw.propagate import _fft_mesh
from pylab import *

def square(x, width=1, center=0):
    y = zeros(len(x - center)) + 1. * (abs(x - center) < width / 2.)
    return y


def pdf(psi):
    return real(conj(psi) * psi)

N = 2 ** 9
L = 60.
width = 4.
x = linspace(-L / 2., L / 2., N)
#psi = square(x, width, 1) / 2
psi = pi ** (-1. / 4) * exp(- x ** 2 / 2.)

ion()
fig = figure()
ax = fig.add_subplot(111)
ylim([-0.8, 0.8])
#xlim([-5, 5])
real_line, = ax.plot(x, real(psi), 'b-')
imag_line, = ax.plot(x, imag(psi), 'r--')
prob_line, = ax.plot(x, pdf(psi), 'k')
show(block=False)
#fig.canvas.draw()

# Set up the differential equation solver
k, dk, x, dx, x0, n = _fft_mesh(x)

def V(t, x):
    return x ** 2 / 2.


def dx0(t):
    return 10

    
def rhs(t, psi):
    return - 1j * hamiltonian(t, psi, x, dx, x0, n, dx0, V)


def jac(t, psi):
    return jacobian(t, x, dx, x0, n, dx0, V)

de = complex_ode(f=rhs)
de.set_integrator('vode', with_jacobian=False)
de.set_initial_value(psi, 0)

# integrate the ode forward
dt = 0.005 * 2 * pi
for index in range(2000):
    de.integrate(
        de.t + dt
    )
    real_line.set_ydata(real(de.y))
    imag_line.set_ydata(imag(de.y))
    prob_line.set_ydata(pdf(de.y))
    fig.canvas.draw()
    
    # Calculate overlap integral
    #overlap = simps(conj(de.y) * de.y, x)
    #print "%.4f\t%.4f" %(index * dt, overlap)
    #time.sleep(0.001)

print "finished"
