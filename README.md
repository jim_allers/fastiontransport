Fast Ion Transport Simulation 
================================

This `python` package contains code to simulate the evolution of ion-transport
waveforms.
